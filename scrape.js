import * as aws from "aws-sdk"
import * as fs from "fs"
import { downloadAndScrape, insert, promiseSerial } from "./src/utils"

aws.config.update({
    region: "us-east-1",
    endpoint: "https://dynamodb.us-east-1.amazonaws.com",
})

const client = new aws.DynamoDB.DocumentClient()

fs.readFile("./scrape_batch.txt", "utf8", (oErr, sText) => {
    const songNumbers = sText.split("\n").filter(l => l !== "")//.slice(0, 1)

    promiseSerial(songNumbers.map(number => () =>
        downloadAndScrape(number)
        .then(r => {
            console.log(r)
            return r
        })
        .then(song => insert(song))))
    .then(() => {
        console.log("All inserted")
    })
})
