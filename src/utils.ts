
const uuid = require("uuid")
const request = require("request")
const cheerio = require("cheerio")

export const promiseSerial = (funcs) =>
    funcs.reduce(
        (promise, func) => promise.then(result => func().then(Array.prototype.concat.bind(result))),
        Promise.resolve([]))

export const downloadAndScrape = (number) => {
    return new Promise((res, rej) => {
        request({ uri: `http://www.supermusic.cz/export.php?idpiesne=${number}&typ=TXT` }, (error, response, body) => {

            const $ = cheerio.load(body)

            $("a").remove()

            const text = $("table tr:nth-child(2) td")
            const lines = text.text().replace("SŤAHUJ", "").split("\n").filter(l => l !== "")
            const content = lines.join("<br>").replace(/\[(.*?)\]/g, (match, p) => `<sup>${p}</sup>`)

            request({ uri: `http://www.supermusic.cz/skupina.php?idpiesne=${number}&sid=` }, (error2, response2, body2) => {

                const $1 = cheerio.load(body2)

                const author = $1("font.interpret").text()
                const title = $1("title").text().replace("[akordy a text na Supermusic]", "").replace(author, "").substr(3).trim()
                const youtubeUrl = $1("iframe.youtube-player").attr("src")

                res({
                    author: author,
                    title: title,
                    youtube: youtubeUrl ? youtubeUrl.replace("https://www.youtube.com/embed/", "").replace("?autoplay=0", "") : null,
                    content: content,
                    supermusic: number,
                })
            })
        })
    })
}
