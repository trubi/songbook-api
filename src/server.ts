import * as AWS from "aws-sdk"
import { ServiceConfigurationOptions } from "aws-sdk/lib/service"
import * as dotenv from "dotenv"
import * as express from "express"
import * as uuid from "uuid"
import * as cors from "cors"
import {downloadAndScrape} from "./utils"

dotenv.config({ path: ".env" })

const port = process.env.PORT || 3011
const app = express()
const router = express.Router()

AWS.config.update({
    region: "us-east-1",
    // endpoint: "https://dynamodb.us-east-1.amazonaws.com",
    accessKeyId: "AKIAYK27ATHTWUICXIXB",
    secretAccessKey: "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"
})

const serviceConfigOptions: ServiceConfigurationOptions = {
    region: "us-east-1",
    endpoint: "https://dynamodb.us-east-1.amazonaws.com",
}

const client = new AWS.DynamoDB.DocumentClient(serviceConfigOptions)

app.use(cors())
app.use(express.json())

router.get("/ping", (req, res) => {
    res.json({})
})

router.get("/", (req, res) => {
    res.json({})
})

router.get("/song", (req, res) => {
    const params = {
        TableName: "songbook_songs",
    }
    client.scan(params, (err, data) => {
        if (data) {
            res.json(data.Items)
        }
        else {
            res.json([])
        }
    })
})

router.post("/song", (req, res) => {
    console.log("writing item")
    console.log(req.body.author)
    console.log(req.body.title)
    console.log(req.body.content)

    const params = {
        TableName: "songbook_songs",
        Key: {
            id: req.body.id,
        },
        UpdateExpression: "set author = :x1, title = :x2, content = :x3",
        ExpressionAttributeValues: {
            ":x1": req.body.author,
            ":x2": req.body.title,
            ":x3": req.body.content,
        },
    }

    // @ts-ignore
    client.update(params, (err, data) => {
        console.log("err", err)
        console.log("data", data)
        res.json({})
    })
})

router.post("/song/scrape", (req, res) => {
    console.log("Scraping...")
    console.log(req.body)

    downloadAndScrape(req.body.songId).then(result => {
        console.log(result)

        return new Promise((res, rej) => {
            const params = {
                TableName: "songbook_songs",
                Item: {
                    id: uuid.v4(),
                    ...result,
                },
            }

            client.put(params, (err, data) => {
                if (err) {
                    rej(err)
                }
                else {
                    res(data)
                }
            })
        })
    }).then(() => {
        res.json({})
    }).catch(err => {
        res.json({ error: err })
    })
})

// router.put("/song", (req, res) => insertSong(client, req, res))

app.use("/", router)

app.listen(port, (err) => {
    if (err) {
        console.log(err)
    }
    console.log(`Server is listening on port ${port}`)
})
