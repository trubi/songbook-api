
import uuid from "uuid"


const insertSong = (client, req, res) => {
    const title = req.query.title
    const song = req.query.content
    const video = req.query.video

    const params = {
        TableName: "songbook_songs",
        Item: {
            id: uuid.v4(),
            name: title,
            video: video,
            content: normalize(song),
        },
    }

    client.put(params, (err, data) => {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2))
        }
        else {
            console.log("Added item:", JSON.stringify(data, null, 2))
        }
    })

    res.json({})
}

const normalize = (song) => song
    .replace(/<a(.*?)">/g, "")
    .replace(/<\/a>/g, "")
    .replace(/<br>/g, "<br />")

export default insertSong
